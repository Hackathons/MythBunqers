#!/usr/bin/env .venv/bin/python -W ignore
import sys
from libs.bunq_lib import BunqLib
from libs.share_lib import ShareLib

def main():
    all_option = ShareLib.parse_all_option()
    environment_type = ShareLib.determine_environment_type_from_all_option(all_option)
    new_user = ShareLib.determine_new_user_from_all_option(all_option)

    ShareLib.print_header()

    bunq = BunqLib(environment_type)
    
    template = {
        'daily_limit': '20 EUR',
        'debt_accounts': [
            {
                'Description': 'Doel: Alimentatie',
                'Amount': '300',
            },
            {
                'Description': 'Doel: Zorgkosten',
                'Amount': '100',
            }
        ]
    }


    counter_user_email = 'donald.mason@bunq.org'
    #Make Main account that receives all income
    main_account = bunq.make_account('Main income account')
    # print(main_account)

    bunq.make_request("500", "init", "sugardaddy@bunq.com")
    bunq.make_request("500", "init", "sugardaddy@bunq.com")
    bunq.make_request("500", "init", "sugardaddy@bunq.com")

    #Make daily payment account that gets a designated amount of income
    daily_allowance_account = bunq.make_account( 'Daily allowance account', template['daily_limit'])
    #print(bunq.get_my_monetary_account(daily_allowance_account.value)._alias[0].value)
    monetary_account = bunq.get_my_monetary_account(daily_allowance_account.value)
    bunq.make_iban_payment("600",
                            monetary_account._alias[0].value,
                            monetary_account._alias[0].name,
                           "Daily allowance"
                           )
    bunq.make_share_request(counter_user_email, daily_allowance_account.value)

    debt_accounts = []
    #Make debt account
    for account in template['debt_accounts']:
        debt_account = bunq.make_account(account['Description'])

        monetary_account = bunq.get_my_monetary_account(debt_account.value)
        bunq.make_iban_payment(
                                account['Amount'],
                                monetary_account._alias[0].value,
                                monetary_account._alias[0].name,
                                "Debt payment"
                            )

        schedule = bunq.create_schedule("WEEKLY")
        recipient = bunq.get_my_monetary_account(debt_account.value)
        payment = bunq.create_payment(
            account['Amount'],
            recipient._alias[0],
            account['Description'] + " " + account['Amount']
        )
        bunq.schedule_payment(main_account.value, payment, schedule)
        #schedule payment
        bunq.make_share_request(counter_user_email, debt_account.value)

if __name__ == '__main__':
    main()
