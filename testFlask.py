from flask import Flask, render_template
from libs.bunq_lib import BunqLib
from libs.share_lib import ShareLib
from bunq.sdk.model.generated.endpoint import *
from bunq.sdk.model.generated.object_ import Pointer, Amount, NotificationFilter
from bunq.sdk.client import Pagination
from datetime import datetime
import json
app = Flask(__name__)


bunq = None
user_id = None
email = None
all_option = ShareLib.parse_all_option()
environment_type = ShareLib.determine_environment_type_from_all_option(all_option)

# all_alias = bunq.get_all_user_alias()
# ShareLib.print_all_user_alias(all_alias)

def open_server_env():
	global bunq
	global user_id
	global email

	bunq = BunqLib(environment_type)
	user_id = bunq.get_current_user()._id_
	email = [f.value for f in bunq.get_current_user()._alias if f.type_ == "EMAIL"][0]


def open_temp_client_env():
	global bunq
	global user_id

	bunq = BunqLib(environment_type, True)
	user_id = bunq.get_current_user()._id_

@app.route("/")
def login():
	return render_template('login.html')

@app.route("/accounts") 
def index():
	portfolios = list()
	persons = list()
	for a in MonetaryAccount.list().value:
		if a._MonetaryAccountBank._user_id != user_id:
    			
			exists = False
			for persoon in persons:
				if persoon["uname"] == a._MonetaryAccountBank._user_id:
					persoon["rekeningen"].append(
						{"id":a._MonetaryAccountBank._id_,"description":a._MonetaryAccountBank._description}
					)
					exists = True
			
			if not exists:
				persons.append(
					{"uname":a._MonetaryAccountBank._user_id,"rekeningen":[{"id":a._MonetaryAccountBank._id_,"description":a._MonetaryAccountBank._description}]}
				)
				name = [f.name for f in a._MonetaryAccountBank._alias if f.type_ == "IBAN" ][0]
				portfolios.append(
					{
						"name":name,
						"uname":a._MonetaryAccountBank._user_id
					}
				)
	return render_template("index.html",
		portfolios=portfolios,
		personen=persons
		)

@app.route("/new_client")
def new_client() -> str:
	open_temp_client_env()
	bunq.create_client( email )
	
	open_server_env()
	return json.dumps({"success":True})


@app.route("/connectRequests")
def getConnectRequests():
	pagination = Pagination()
	pagination.count = 5
	a = ShareInviteBankResponse.list(params=pagination.url_params_count_only).value
	for b in a:
		print(b.__dict__)
		print(b._counter_alias.__dict__)
		print(b._share_detail.__dict__)
	return ""

@app.route("/get_info/<name>/<id>")
def get_info(name: str, id: str) -> str:
	account = MonetaryAccountBank.get(int(id)).value
	print(account.__dict__)
	created = datetime.strptime(account._created, "%Y-%m-%d %H:%M:%S.%f").strftime("%d-%m-%Y")
	name = [f.name for f in account._alias if f.type_ == "IBAN" ][0]
	madeFor = name
	balance = account._balance.value if account._balance else "No permission"
	print(account.__dict__)
	dailyLimit = account._daily_limit.value
	pagination = Pagination()
	pagination.count = 5
	trans = list()
	a = Payment.list(int(id), params=pagination.url_params_count_only)
	if a is not None:
		trans = a.value

	transactions = ""
	for tran in trans:
		print(tran.__dict__)
		print(tran._alias.__dict__)
		print(tran._alias.label_monetary_account.__dict__)
		print(tran._alias.pointer.__dict__)
		transactions += '''
			<hr>
			<h4>{4} {5} {0}<h4>
			<h6>{1}</h6>
			<span style="padding-left:2em;"><b>Amount: {2}</b></span>
			<br/>
			{3}
			<hr>
		'''.format(
			tran._counterparty_alias.pointer.name,
			tran._counterparty_alias.pointer.value,
			'<span style="color: green">' + str(tran._amount.value) + '</span>' if '-' not in tran._amount.value else '<span style="color: red">' + str(tran._amount.value) + '</span>',
			tran._description,
			tran._sub_type.capitalize(),
			"to" if tran._sub_type == "PAYMENT" else "from"
		)
	print(transactions)
	return json.dumps({"created":created,"madeFor":madeFor,"balance":balance,"dailyLimit":dailyLimit,"trans":transactions})    
    	
open_server_env()
app.run(debug=True)