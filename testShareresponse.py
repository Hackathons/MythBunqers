from libs.bunq_lib import BunqLib
from libs.share_lib import ShareLib
from bunq.sdk.model.generated.endpoint import *
from bunq.sdk.model.generated.object_ import Pointer, Amount, NotificationFilter
from bunq.sdk.client import Pagination
all_option = ShareLib.parse_all_option()
environment_type = ShareLib.determine_environment_type_from_all_option(all_option)
bunq = BunqLib(environment_type)
[print(a.__dict__) for a in ShareInviteBankResponse.list().value]